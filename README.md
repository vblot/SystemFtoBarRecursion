# SystemFtoBarRecursion
Implementations of an interpretation of system F through bar recursion defined in:
https://valentinblot.org/pro/SystFbarrec.pdf
